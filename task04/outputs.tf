output "resource_group_name" {
  value = azurerm_resource_group.rg.name
}

output "virtual_network_name" {
  value = azurerm_virtual_network.vnet.name
}

output "subnet_name" {
  value = azurerm_subnet.subnet.name
}

output "nic_name" {
  value = azurerm_network_interface.nic.name
}

output "public_ip_address" {
  value = azurerm_public_ip.public_ip.ip_address
}

output "virtual_machine_name" {
  value = azurerm_linux_virtual_machine.vm.name
}

output "network_security_group_name" {
  value = azurerm_network_security_group.nsg.name
}

output "dns_name" {
  value = azurerm_public_ip.public_ip.domain_name_label
}
