variable "location" {
  type        = string
  default     = "eastus"
  description = "Location for all resources."
}

variable "rg-name" {
  type    = string
  default = "cmtr-6ab30b9f-rg"
}

variable "vnet-name" {
  type    = string
  default = "cmtr-6ab30b9f-vnet"
}

variable "subnet-name" {
  type    = string
  default = "public-subnet"
}

variable "pip-name" {
  type    = string
  default = "cmtr-6ab30b9f-publicip"
}

variable "nic-name" {
  type    = string
  default = "cmtr-6ab30b9f-nic"
}

variable "nsg-name" {
  type    = string
  default = "cmtr-6ab30b9f-nsg"
}

variable "vm-name" {
  type    = string
  default = "cmtr-6ab30b9f-vm"
}

variable "dns-name" {
  type    = string
  default = "cmtr-6ab30b9f-nginx"
}

variable "admin_username" {
  type    = string
  default = "shtajiryan"
}

variable "admin_password" {
  type        = string
  description = "Admin password"
  default     = "Temppass123"
  sensitive   = true
}

variable "ssh_rule_name" {
  type    = string
  default = "cmtr-22"
}

variable "http_rule_name" {
  type    = string
  default = "cmtr-80"
}
